﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using BackendlessAPI;
using BackendlessAPI.Async;

namespace internXChange.Functions
{
    public class userFunctions
    {

        //--------------//
        //USER REGISTRATION FUNCTION//
        //--------------//

        public async static Task<string> userRegister(string email, string firstName, string lastName, byte[] pass)
        {
            BackendlessUser reguser = new BackendlessUser();
            reguser.AddProperty("email", email);
            reguser.AddProperty("firstName", firstName);
            reguser.AddProperty("lastName", lastName);
            reguser.Password = System.Text.Encoding.UTF8.GetString(pass);
            string ret = "";
            AsyncCallback<BackendlessUser> callback = new AsyncCallback<BackendlessUser>(
                registeredUser =>
                {
                    ret = "true";
                },
                error =>
                {
                    ret = error.Message;
                });

            Backendless.UserService.Register(reguser, callback);
            while(ret == "")
            {
                await Task.Delay(25);
            }
            return ret;
        }


        //--------------//
        //USER LOGIN FUNCTION//
        //--------------//


        public async static Task<string> userLogin(string email, byte[] pass, bool stayedlogin)
        {
            string ret = "";
            AsyncCallback<BackendlessUser> callback = new AsyncCallback<BackendlessUser>(
             user =>
            {
                ret = "true";
            },
            error =>
            {
                ret = error.Message;
            });
            Backendless.UserService.Login(email, System.Text.Encoding.UTF8.GetString(pass), callback , stayedlogin);
            while (ret == "")
            {
                await Task.Delay(25);
            }
            return ret;

        }


        //--------------//
        //USER LOGOUT FUNCTION//
        //--------------//


        public async static Task<string> LogOut()
        {
            string ret = "";
            AsyncCallback<Object> logoutCallback = new AsyncCallback<Object>(
            user =>
            {
                ret = "true";
            },
            error =>
            {
                ret = error.FaultCode;
            });

            Backendless.UserService.Logout(logoutCallback);
            while (ret == "")
            {
                await Task.Delay(25);
            }
            return ret;
        }
    }
}
