using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using BackendlessAPI;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace internXChange
{
    public class Startup
    {
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseDefaultFiles();
            app.UseStaticFiles();
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
          
            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapPost("/register", async context =>
                {
                    string fname = context.Request.Form["fName"];
                    string lname = context.Request.Form["lName"];
                    string email = context.Request.Form["email"];
                    byte[] password = UTF8Encoding.UTF8.GetBytes(context.Request.Form["password"]);
                    Backendless.InitApp("FC3A171F-07C5-D641-FF3D-2E65AA724D00", "206CBDE6-D3B8-45CF-8AF9-3BA883FEB2A3");
                    Task<string> result = Functions.userFunctions.userRegister(email, fname, lname, password);
                    result.Wait();
                    context.Response.Headers["result"] = result.Result;



                });
                endpoints.MapGet("/register", async context =>
                {
                    await context.Response.WriteAsync(System.IO.File.ReadAllText("register.html"));
                    
                });
                endpoints.MapGet("/register.html", async context =>
                {
                    await context.Response.WriteAsync(System.IO.File.ReadAllText("register.html"));

                });
                endpoints.MapGet("/login", async context =>
                {
                    await context.Response.WriteAsync(System.IO.File.ReadAllText("login.html"));

                });
                endpoints.MapGet("/", async context =>
                {
                    await context.Response.WriteAsync(System.IO.File.ReadAllText("index.html"));

                });
                endpoints.MapGet("/index", async context =>
                {
                    await context.Response.WriteAsync(System.IO.File.ReadAllText("index.html"));

                });
                endpoints.MapGet("/index.html", async context =>
                {
                    await context.Response.WriteAsync(System.IO.File.ReadAllText("index.html"));

                });
                endpoints.MapGet("/login-1.html", async context =>
                {
                    await context.Response.WriteAsync(System.IO.File.ReadAllText("login-1.html"));

                });
            });
        }
    }
}
